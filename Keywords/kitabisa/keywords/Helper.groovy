package kitabisa.keywords

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.testobject.SelectorMethod
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.Select

import internal.GlobalVariable as GlobalVariable
import java.nio.charset.StandardCharsets;
import java.nio.file.*;

public class Helper extends Common {

	@Keyword
	public static getResolution = {
		def size = driver.manage().window().getSize().toString()
		println("Your resolution browser is : ".concat(size))
	}

	private static List<String> influencedKeywords = [
		'click',
		'selectOptionByIndex',
		'selectOptionByLabel',
		'selectOptionByValue',
		'setEncryptedText',
		'setText',
		'sendKeys',
		'waitForElementVisible',
		'executeScript',
		'waitForElementNotVisible',
		'uploadFile',
		'convertWebElementToTestObject',
		'findWebElements',
		'verifyElementVisible'
	]

	@Keyword
	def static TestObject defineObject(String xPath){
		TestObject to = new TestObject('testingObject')
		to.addProperty("xpath", ConditionType.EQUALS, xPath)
		return to
	}

	/**
	 * change some of methods of WebUiBuiltInKeywords so that they call HighlightElement.on(testObject)
	 * before invoking their original method body.
	 *
	 * http://docs.groovy-lang.org/latest/html/documentation/core-metaprogramming.html#metaprogramming
	 */
	@Keyword
	public static void pandemic() {
		WebUI.metaClass.'static'.invokeMethod = { String name, args ->
			if (name in influencedKeywords) {
				TestObject to = (TestObject)args[0]
				Helper.influence(to)
				WebUI.takeScreenshot();
			}
			def result
			try {
				result = delegate.metaClass.getMetaMethod(name, args).invoke(delegate, args)
			} catch(Exception e) {
				System.out.println("Handling exception for method $name")
			}
			return result
		}
	}

	private static void influence(TestObject testObject) {
		try {
			List<WebElement> elements = WebUiCommonHelper.findWebElements(testObject, 20);
			for (WebElement element : elements) {
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript(
						"arguments[0].setAttribute('style','outline: dashed red;');",
						element);
			}
		} catch (Exception e) {
			// TODO use Katalon Logging
			e.printStackTrace()
		}
	}


}