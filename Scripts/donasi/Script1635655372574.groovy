import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import kitabisa.keywords.Common as Common
import kitabisa.keywords.Helper as Helper
import org.openqa.selenium.Keys

WebUI.openBrowser(GlobalVariable.URL)

Common.setUp()
WebUI.maximizeWindow()

WebUI.delay(1)

WebUI.scrollToElement(Helper.defineObject("//h3[normalize-space()='Pilih Kategori Favoritmu']/following-sibling::div"), 1)
WebUI.click(Helper.defineObject("//h3[normalize-space()='Pilih Kategori Favoritmu']/following::a[1]/div[1]"))
assert WebUI.verifyElementVisible((Helper.defineObject("//button[@id='campaign-detail_button_donasi-sekarang']")), FailureHandling.STOP_ON_FAILURE) == true

WebUI.click(Helper.defineObject("//button[@id='campaign-detail_button_donasi-sekarang']"))
assert WebUI.verifyElementVisible((Helper.defineObject("//p[contains(text(),'Masukkan Nominal')]")), FailureHandling.STOP_ON_FAILURE) == true

WebUI.click(Helper.defineObject("//label[normalize-space()='Rp 10.000']"))
assert WebUI.verifyElementVisible((Helper.defineObject("//span[contains(text(),'Metode Pembayaran')]")), FailureHandling.STOP_ON_FAILURE) == true
WebUI.scrollToElement(Helper.defineObject("//label[contains(text(),'Transfer bank')]"), 1)
WebUI.click(Helper.defineObject("//span[contains(text(),'Transfer BCA')]"))

WebUI.sendKeys(findTestObject('input_name'), "firmansyah QA")

WebUI.sendKeys(findTestObject('input_phonenumber_or_email'), "081234567890")

WebUI.click(Helper.defineObject("//span[@data-testid='button-lanjut-pembayaran']/parent::button"))
assert WebUI.verifyElementVisible((Helper.defineObject("//h2[normalize-space()='Instruksi Pembayaran']")), FailureHandling.STOP_ON_FAILURE) == true

WebUI.delay(1)


