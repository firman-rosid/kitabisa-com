
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import java.lang.String



def static "kitabisa.keywords.Helper.defineObject"(
    	String xPath	) {
    (new kitabisa.keywords.Helper()).defineObject(
        	xPath)
}

/**
	 * change some of methods of WebUiBuiltInKeywords so that they call HighlightElement.on(testObject)
	 * before invoking their original method body.
	 *
	 * http://docs.groovy-lang.org/latest/html/documentation/core-metaprogramming.html#metaprogramming
	 */
def static "kitabisa.keywords.Helper.pandemic"() {
    (new kitabisa.keywords.Helper()).pandemic()
}

/**
	 * Base variable of Helper
	 */
def static "kitabisa.keywords.Common.setUp"() {
    (new kitabisa.keywords.Common()).setUp()
}
