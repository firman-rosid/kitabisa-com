import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

import kitabisa.keywords.Common
import kitabisa.keywords.Helper
import com.kms.katalon.core.annotation.AfterTestCase
import com.kms.katalon.core.annotation.AfterTestSuite
import com.kms.katalon.core.annotation.BeforeTestCase
import com.kms.katalon.core.annotation.BeforeTestSuite
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

class Listener {
	@BeforeTestCase
	def beforeTestCase() {
		Helper.pandemic()
	}
	
	@AfterTestCase
	def afterTestCase() {
		WebUI.takeScreenshot()
		Helper.getResolution()
		WebUI.closeBrowser()
	}
}